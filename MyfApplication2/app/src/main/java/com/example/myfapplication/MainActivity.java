package com.example.myfapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView NewText;
    Button ChangeText;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ChangeText = (Button) findViewById(R.id.firstButton);
        NewText = (TextView) findViewById(R.id.textView);
    }

    public void pressMeToChangeText(View view){
        NewText.setText("What's going on there?");
    }

    public void pressToRestMe(View view){
        NewText.setText("I am back! Hello there.");
    }
}