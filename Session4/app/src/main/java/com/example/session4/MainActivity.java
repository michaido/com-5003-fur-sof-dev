package com.example.session4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i(TAG, "onCreate");

        Button savebtn = findViewById(R.id.saveButton);
        Button loadbtn = findViewById(R.id.loadButton);
    }

    public void onClickSave(View view){
        EditText lastName_edit = findViewById(R.id.fieldLastName);
        EditText firstName_edit = findViewById(R.id.fieldFirstName);

        SharedPreferences sp = getSharedPreferences("MY_PREFERENCES",MODE_PRIVATE);
        SharedPreferences.Editor myEdit = sp.edit();
        String lastName = lastName_edit.getText().toString();
        String firstName = firstName_edit.getText().toString();

        myEdit.putString("lastname",lastName);
        myEdit.putString("firstname", firstName);
        myEdit.apply();

        Toast.makeText(MainActivity.this, "Saved it", Toast.LENGTH_SHORT).show();
        Log.i(TAG, "onClickSave: Toast is an Android class which is used to display messages to the user");

    }

    public void onClickLoad(View view){
        TextView name_text = findViewById(R.id.spText);

        SharedPreferences sp = getSharedPreferences("MY_PREFERENCES", MODE_PRIVATE);
        String lastName = sp.getString("lastname","na");
        String firstName = sp.getString("firstname", "na");

        name_text.setText(String.format("%s, %s",lastName, firstName));
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy");
    }

}