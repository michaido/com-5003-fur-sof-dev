package com.example.logintemplate;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class Database extends SQLiteOpenHelper{

    private static final String DB_NAME = "logindb";

    private static final int DB_VERSION = 1;

    private static final String TABLE_NAME = "users";

    private static final String ID_COL = "id";

    private static final String USER_EMAIL = "email";

    private static final String USER_PASS = "password";

    public Database(@Nullable Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE " + TABLE_NAME + " ("
                + ID_COL + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + USER_EMAIL + " TEXT,"
                + USER_PASS + " TEXT)";

        db.execSQL(query);
    }

    public void addNewUser(String email, String password) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(USER_EMAIL, email);
        values.put(USER_PASS, password);

        db.insert(TABLE_NAME, null, values);

        db.close();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
}
