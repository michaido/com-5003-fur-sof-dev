package com.example.ltufitnessdesign;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class profileActivity extends AppCompatActivity {

    DataBaseActivity ltuDB = new DataBaseActivity(this);

    static final int COLUMROW = 3;

    TextView username, email, password;
    ImageView backButton;


    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        username = findViewById(R.id.usernameId);
        email = findViewById(R.id.emailId);
        password = findViewById(R.id.passwordId);
        backButton = findViewById(R.id.backBtnId);

        Intent home = getIntent();
        String usernameInfo = home.getStringExtra("username");

        String userInfo[] = null;
        userInfo = getUserInfo(usernameInfo);

        username.setText("Username: " + userInfo[0]);
        email.setText("Email: " + userInfo[1]);
        password.setText("Password: " + userInfo[2]);


        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent homeView = new Intent(profileActivity.this, HomeActivity.class);
                startActivity(homeView);
            }
        });

    }

    public String[] getUserInfo(String username){

        String userInfoQuery = "SELECT username, email, password FROM user WHERE username='" +
                username + "'";

        Cursor userInfoCursor = ltuDB.selectOps(userInfoQuery);

        //System.out.println("###### " + resSize);

        String userInfoArray[] = new String[COLUMROW];


        if(userInfoCursor.moveToFirst()){
            userInfoArray[0] = userInfoCursor.getString(0);
            userInfoArray[1] = userInfoCursor.getString(1);
            userInfoArray[2] = userInfoCursor.getString(2);
        }

        return userInfoArray;
    }

}