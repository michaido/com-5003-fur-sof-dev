package com.example.ltufitnessdesign;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RegistrationActivity extends AppCompatActivity {

    EditText username, email, password, conPassword;
    Button register;
    TextView alreadySignedUp;

    DataBaseActivity ltuDB = new DataBaseActivity(this);
    private static int PASSWORD_LENGTH = 6;

    UserInfo user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        username = findViewById(R.id.userName);
        email = findViewById(R.id.userEmail);
        password = findViewById(R.id.userPassword);
        conPassword = findViewById(R.id.userPasswordConfirmation);

        register = findViewById(R.id.userSignUp);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addUsers();
            }
        });

        alreadySignedUp = findViewById(R.id.alreadySignup);
        alreadySignedUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginPage = new Intent(RegistrationActivity.this, LoginActivity.class);
                startActivity(loginPage);
            }
        });

    }

    private void addUsers(){

        String localUsername = username.getText().toString().trim();
        String localUserEmail = email.getText().toString().trim();
        String localPassword = password.getText().toString().trim();
        String localConPassword = conPassword.getText().toString().trim();



        if (localUsername.isEmpty()) {
            username.setError("Username is required");
            username.requestFocus();
            return;
        }

        if (localUserEmail.isEmpty()) {
            email.setError("Email is required");
            email.requestFocus();
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(localUserEmail).matches()) {
            email.setError("Please provide a valid username!");
            email.requestFocus();
            return;
        }

        if (ltuDB.authEmail(localUserEmail, localPassword)) {
            email.setError("Email already exists!Please provide a different email.");
            email.requestFocus();
            return;
        }

        if (localPassword.isEmpty()) {
            password.setError("Password is required");
            password.requestFocus();
            return;
        }
        if (localPassword.length() < PASSWORD_LENGTH) {
            password.setError("Password is required");
            password.requestFocus();
            return;
        }
        if (localPassword.compareTo(localConPassword) != 0) {
            conPassword.setError("The Confirm Password does not match the password");
            conPassword.requestFocus();
            return;
        }

        user = new UserInfo();
        user.setUsername(username.getText().toString());
        user.setEmail(email.getText().toString());
        user.setPassword(password.getText().toString());

        boolean result = ltuDB.addUser(user);
        if (result == true){
            Toast.makeText(getApplicationContext(), "User successful added!", Toast.LENGTH_SHORT).show();
            Intent loginPage = new Intent(RegistrationActivity.this, LoginActivity.class);
            startActivity(loginPage);
        }
        else if (result == false) {
            Toast.makeText(getApplicationContext(), "Sorry user not added!", Toast.LENGTH_SHORT).show();
        }
        username.setText("");
        email.setText("");
        password.setText("");
    }

}