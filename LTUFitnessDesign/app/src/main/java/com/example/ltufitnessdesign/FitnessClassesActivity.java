package com.example.ltufitnessdesign;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FitnessClassesActivity extends AppCompatActivity {

    ListView listView;
    DataBaseActivity db = new DataBaseActivity(this);
    ImageView backBtn;
    Intent booking = getIntent();
    //booking




    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fitness_classes);


        db.onCreate(db.getWritableDatabase());

        Map<String, ArrayList> setClasses = new HashMap<String, ArrayList>();

        setClasses  = db.getClassDetails();

        List className = new ArrayList();
        HashMap<String, String> tempClass;
        String tempVals[];

        for(int i=1; i<= setClasses.size(); i++) {
            String key = String.valueOf(i);
            ArrayList<String> valuse = new ArrayList<String>();
            valuse = setClasses.get(key);
            tempVals = valuse.toArray(new String[valuse.size()]);

            tempClass = new HashMap<String, String>();
            tempClass.put("id", tempVals[0]);
            tempClass.put("Name", tempVals[1]);
            tempClass.put("Instructor", "Instructor: " + tempVals[5]);
            tempClass.put("Date", "Date: " + tempVals[2]);
            tempClass.put("Time", "Time: " + tempVals[3]);
            tempClass.put("Availability", "Available Space: " + tempVals[7]);
            tempClass.put("Duration", "Duration: " + tempVals[4] + "minutes");
            className.add(tempClass);

        }

        listView = findViewById(R.id.listView);
        SimpleAdapter adapter = new SimpleAdapter(this,className,R.layout.valuelines,
                new String[]{"Name", "Instructor", "Date", "Time", "Availability"},
                new int[]{R.id.ClassName, R.id.ClassInstructor, R.id.ClassDate, R.id.ClassTime, R.id.ClassAvailability});


        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent item = new Intent(FitnessClassesActivity.this, BookFitnessClasses.class);
                HashMap<String, String> classInfo = (HashMap<String, String>) className.get(position);

                item.putExtra("id", classInfo.get("id"));
                item.putExtra("name",classInfo.get("Name"));
                item.putExtra("date",classInfo.get("Date"));
                item.putExtra("time",classInfo.get("Time"));
                item.putExtra("instructor",classInfo.get("Instructor"));
                item.putExtra("duration",classInfo.get("Duration"));
                item.putExtra("availability",classInfo.get("Availability"));
                Intent homeIntent = getIntent();
                String username = homeIntent.getStringExtra("username");
                System.out.println("username value from Home: " + username);
                item.putExtra("username", username);

                startActivity(item);

            }
        });

        backBtn = findViewById(R.id.backhome_classes);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent back = new Intent(FitnessClassesActivity.this, HomeActivity.class);
                startActivity(back);
            }
        });
    }

}