package com.example.ltufitnessdesign;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

public interface DataOperationManager {
    //public void createTable(String );
    public boolean selectData(String selectQuery);
    public boolean insertData(ContentValues values);
    public boolean updateData(String updateQuery);
    public boolean deleteData(String deleteQuery);
}
