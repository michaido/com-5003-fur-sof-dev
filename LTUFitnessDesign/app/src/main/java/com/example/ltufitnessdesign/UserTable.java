package com.example.ltufitnessdesign;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class UserTable implements DataOperationManager{

    SQLiteDatabase db;

    private static final String TABLE_NAME = "user";

    private static final String COLUMN_ID = "id";

    private static final String FIELD_USERNAME = "username";

    private static final String FIELD_EMAIL = "email";

    private static final String FIELD_PASSWORD = "password";


    UserTable(SQLiteDatabase db){
        this.db = db;
    }

    @Override
    public boolean selectData(String selectQuery) {
        //db = getReadableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);

        return false;
    }

    @Override
    public boolean insertData( ContentValues values) {

        return false;
    }

    @Override
    public boolean updateData(String updateQuery) {

        return false;
    }

    @Override
    public boolean deleteData(String deleteQuery) {

        return false;
    }
}
