package com.example.ltufitnessdesign;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;

import java.time.Instant;

public class HomeActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        CardView logout = findViewById(R.id.cardLogout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent out = new Intent(HomeActivity.this, LoginActivity.class);
                startActivity(out);

            }
        });

        CardView classes = findViewById(R.id.cardBooking);
        classes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent classes = new Intent(HomeActivity.this, FitnessClassesActivity.class);
                Intent item = getIntent();
                String username = item.getStringExtra("username");
                classes.putExtra("username", username);
                //System.out.println("username value from Home: " + username);
                startActivity(classes);
            }
        });

        CardView profile = findViewById(R.id.cardUserActivity);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent profile = new Intent(HomeActivity.this, profileActivity.class);
                Intent item = getIntent();
                String username = item.getStringExtra("username");
                profile.putExtra("username", username);
                startActivity(profile);
            }
        });
    }



}