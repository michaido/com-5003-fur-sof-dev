package com.example.ltufitnessdesign;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    private EditText userName, userPassword;
    private TextView forgotPassword;
    Button login, back_btn, signUp;
    DataBaseActivity ltuDB = new DataBaseActivity(this);

    private static int PASSWORD_LENGTH = 6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        userName = findViewById(R.id.loginUsername);
        userPassword = findViewById(R.id.userLoginPassword);

        forgotPassword = findViewById(R.id.forgotPassword);
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onForgotPassword(v);
            }
        });
        login = findViewById(R.id.userLoginButton);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

        signUp = findViewById(R.id.userSignUpButton2);

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signup = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(signup);
            }
        });


    }

    public void onForgotPassword(View view){
        Intent onForgotPassword = new Intent(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
        startActivity(onForgotPassword);
    }

    public void pressToReturnBack(View view){
        Intent backIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(backIntent);
    }

    public void login(){

        String username = userName.getText().toString().trim();
        String password = userPassword.getText().toString().trim();

        if (username.isEmpty()) {
            userName.setError("Username is required");
            userName.requestFocus();
            return;
        }
        if (!ltuDB.authUsername(username, password)) {
            userName.setError("New Username! Click register button to sign up.");
            userName.requestFocus();
            return;
        }
        if (password.isEmpty()) {
            userPassword.setError("Password is required");
            userPassword.requestFocus();
            return;
        }
        if (password.length() < PASSWORD_LENGTH) {
            userPassword.setError("Password is required");
            userPassword.requestFocus();
            return;
        }

        if (!ltuDB.authPassword(username, password)) {
            userPassword.setError("Password is incorrect! Please Try again.");
            userPassword.requestFocus();
            return;
        }

        if (ltuDB.userExists(username, password)){
            //String userFullName = ltuDB.getUserName(username);
            //Log.d("Successful login", userFullName);
            try {
                Intent home = new Intent(LoginActivity.this, HomeActivity.class);
                home.putExtra("username", username);
                startActivity(home);
            } catch (Exception e){
                Log.d("Error Msg ", e.toString());
            }

        }
        else {
            Toast.makeText(this, "login could not be completed, please try again", Toast.LENGTH_LONG).show();

        }

    }
}