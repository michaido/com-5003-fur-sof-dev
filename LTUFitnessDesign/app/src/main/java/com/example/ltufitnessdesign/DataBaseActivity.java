package com.example.ltufitnessdesign;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class DataBaseActivity extends SQLiteOpenHelper {
    static SQLiteDatabase db;

    private static final String DATABASE_NAME = "fitnessDB.db";

    // User Table info
    private static final String TABLE_NAME = "user";
    private static final String COLUMN_ID = "id";
    private static final String FIELD_USERNAME = "username";
    private static final String FIELD_EMAIL = "email";
    private static final String FIELD_PASSWORD = "password";

    //Classes table
    private static final String TABLE_CLASSES = "classes";
    private static final String CLASS_COLUMN_ID = "id";
    private static final String CLASS_NAME = "name";
    private static final String CLASS_DATE = "date";
    private static final String CLASS_DURATION = "duration";
    private static final String CLASS_TIME = "time";
    private static final String CLASS_INSTRUCTOR = "instructor";
    private static final String CLASS_CAPACITY = "capacity";
    private static final String CLASS_AVAILABILITY = "availability";

    //User_Classes table
    private static final String TABLE_USER_CLASSES = "user_classes";
    private static final String USER_CLASSES_ID = "id";
    private static final String USER_CLASSES_USERID = "userId";
    private static final String USER_CLASSES_USERNAME = "username";
    private static final String USER_CLASSES_CLASSESID = "classesId";
    private static final String USER_CLASSES_CLASSESNAME = "classesName";
    private static final String USER_CLASSES_STATUS = "status";

    private static final int DATABASE_VERSION = 1;

    private static final String CREATE_TABLE = "create table IF NOT EXISTS " + TABLE_NAME +
            " ( " + COLUMN_ID + " integer primary key not null , " +
            FIELD_USERNAME  + " text not null , " +
            FIELD_EMAIL + " text not null," + FIELD_PASSWORD +  " text not null);" ;

    //DROP USER QUERY TABLE
    String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_CLASSES;

    //CLASSES QUERY
    private static final String CREATE_CLASSES_TABLE = " create table IF NOT EXISTS " + TABLE_CLASSES +
            " ( " + CLASS_COLUMN_ID + " integer not null , " +
            CLASS_NAME + " text not null , " + CLASS_DATE + " text not null , " + CLASS_TIME + " text not null , " +
            CLASS_DURATION + " text not null , " + CLASS_INSTRUCTOR + " text not null , " +
            CLASS_CAPACITY + " integer not null , "  + CLASS_AVAILABILITY + " integer not null , " +
            "Primary key " + " ( " + CLASS_COLUMN_ID + ", " +  CLASS_NAME + "));" ;

    // USER_CLASSES QUERY
    private static final String CREATE_USER_CLASSES_TABLE =
            " CREATE TABLE IF NOT EXISTS " + TABLE_USER_CLASSES + " ( " + USER_CLASSES_ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                    USER_CLASSES_USERID + " INTEGER NOT NULL , " +  USER_CLASSES_USERNAME + " TEXT NOT NULL , " +
                    USER_CLASSES_CLASSESID + " INTEGER NOT NULL , " + USER_CLASSES_CLASSESNAME + " TEXT NOT NULL , " +
                    USER_CLASSES_STATUS + " TEXT NOT NULL , " +
                    " FOREIGN KEY ( " + USER_CLASSES_USERID + " ) REFERENCES " + TABLE_NAME + "  , " +
                    " FOREIGN KEY ( " + USER_CLASSES_CLASSESID + " ) REFERENCES " + TABLE_CLASSES + ");";


    DataBaseActivity(Context context){

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
        //Log.d("Table created", TABLE_NAME);
        //db.execSQL(DROP_TABLE);
        db.execSQL(CREATE_CLASSES_TABLE);
        if(selecAllClasses() == false)
            addClasses();

        db.execSQL(CREATE_USER_CLASSES_TABLE);
        //Log.d("Table created", TABLE_USER_CLASSES);
        this.db = db;

    }

    public Boolean selecAllClasses(){
        SQLiteDatabase db = getReadableDatabase();
        String selecAllClassesQuery = "SELECT * FROM classes";

        Cursor cursor = db.rawQuery(selecAllClassesQuery, null);

        return cursor.moveToFirst();

    }
    // add new user in the user table to access the application functionalities
    public boolean addUser(UserInfo user){
        SQLiteDatabase db = getWritableDatabase();

        ContentValues classValues = new ContentValues();
        classValues.put(FIELD_USERNAME, user.getUsername());
        classValues.put(FIELD_EMAIL, user.getEmail());
        classValues.put(FIELD_PASSWORD, user.getPassword());

        try{
            if (!userExists(user.getEmail(), user.getPassword())){
                db.insert(TABLE_NAME, null, classValues);
                Log.d("Insert SUCCESS", classValues.toString());
                return true;
            } else {
                Log.e("Add user message: ", "User already exist: " + user.getEmail());
                return false;
            }

        }catch (Exception e) {
            Log.d("Insert FAILURE", e.toString());
            return false;
        }
    }

    // Check and avoid having duplicate user in the user Table
    public boolean userExists(String user, String password){
        SQLiteDatabase db = getReadableDatabase();

        String fetchuser = "Select " + FIELD_USERNAME + ", " + FIELD_PASSWORD + " from " + TABLE_NAME;
        Cursor cursor = db.rawQuery(fetchuser, null);
        String a,b ="not found";
        Log.d("received user", user);
        Log.d("cursor count", String.valueOf(cursor.getCount()));

        if(cursor.moveToFirst()){
            Log.d("Select ", " clause");
            do{
                a = cursor.getString(0);
                Log.d("a ", a);
                if(a.equals(user)){
                    Log.d("user If loop", a);
                    b = cursor.getString(1);
                    Log.d("b ", b);
                    break;
                }
            }
            while(cursor.moveToNext());
        }
        if (b.equals(password)) {
            Log.d("Returning ", " true");
            return true;
        }
        else return false;
    }

    public boolean authEmail(String email, String password){

        SQLiteDatabase db = getReadableDatabase();

        String fetchuser = "Select " + FIELD_EMAIL + ", " + FIELD_PASSWORD + " from " + TABLE_NAME;
        Cursor cursor = db.rawQuery(fetchuser, null);
        String a,b ="not found";
        Log.d("received email", email);
        Log.d("cursor count", String.valueOf(cursor.getCount()));

        if(cursor.moveToFirst()){
            Log.d("Select ", " clause");
            do{
                a = cursor.getString(0);
                Log.d("a ", a);
                if(a.equals(email)){
                    Log.d("email If loop", a);
                    b = cursor.getString(1);
                    Log.d("b ", b);
                    break;
                }
            }
            while(cursor.moveToNext());
        }
        if (!b.equals(password)) {
            //Log.d("Returning ", " false");
            return false;
        }
        else return true;
    }

    public boolean authUsername(String username){

        SQLiteDatabase db = getReadableDatabase();

        boolean authRes = false;

        String fetchuser = "Select " + FIELD_USERNAME + ", " + FIELD_PASSWORD + " from " + TABLE_NAME;
        Cursor cursor = db.rawQuery(fetchuser, null);
        String a,b ="not found";
        Log.d("received username", username);
        Log.d("cursor count", String.valueOf(cursor.getCount()));

        if(cursor.moveToFirst()){
            Log.d("Select ", " clause");

            do{
                a = cursor.getString(0);
                Log.d("a ", a);
                if(a.equals(username)){
                    authRes = true;
                    break;
                }
            } while(cursor.moveToNext());

        }
        if (authRes) {
            return true;
        }
        else{
            return false;
        }

    }

    public boolean authPassword(String username, String password){

        SQLiteDatabase db = getReadableDatabase();

        String fetchuser = "Select " + FIELD_USERNAME + ", " + FIELD_PASSWORD +" from " + TABLE_NAME;
        Cursor cursor = db.rawQuery(fetchuser, null);
        String a,b ="not found";
        Log.d("received username", username);
        Log.d("cursor count", String.valueOf(cursor.getCount()));

        if(cursor.moveToFirst()){
            Log.d("Select ", " clause");
            do{
                a = cursor.getString(0);
                Log.d("a ", a);
                if(a.equals(username)){
                    Log.d("username If loop", a);
                    b = cursor.getString(1);
                    Log.d("b ", b);
                    break;
                }
            }
            while(cursor.moveToNext());
        }
        if (!b.equals(password)) {
            return false;
        }
        else return true;
    }

    public boolean checkEmailExist(String email, String password){

        SQLiteDatabase db = getReadableDatabase();

        String fetchuser = "Select " + FIELD_EMAIL + " from " + TABLE_NAME;
        Cursor cursor = db.rawQuery(fetchuser, null);
        String a,b ="not found";
        Log.d("received email", email);
        Log.d("cursor count", String.valueOf(cursor.getCount()));

        boolean checkResult = false;

        if(cursor.moveToFirst()){
            do{
                a = cursor.getString(0);
                Log.d("a ", a);
                if(a.equals(email)) {
                    checkResult = true;
                    System.out.println("Email found: " + a);
                    break;
                }
            }
            while(cursor.moveToNext());
        }
        if (checkResult) {
            Log.d(" Verify email", "Email exist!");
            return true;
        }
        else return false;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE);
        this.onCreate(db);

    }

    public Cursor selectOps(String selectQuery){
        SQLiteDatabase db = getReadableDatabase();
        db.beginTransaction();
        try {
            Cursor cursor = db.rawQuery(selectQuery, null);
            db.setTransactionSuccessful();
            return cursor;
        } catch (Exception e) {
            Log.e("selectOps", "Failed to select info");
            return null;
        } finally {
            db.endTransaction();
        }

    }

    public Map getClassDetails (){

        SQLiteDatabase db = getReadableDatabase();
        String  columnsName[] = {CLASS_COLUMN_ID, CLASS_NAME, CLASS_DATE, CLASS_TIME, CLASS_DURATION, CLASS_INSTRUCTOR, CLASS_CAPACITY, CLASS_AVAILABILITY};


        db.beginTransaction();
        try {
            Cursor cursor = db.query(TABLE_CLASSES, columnsName, null,null,null,null,null);
            db.setTransactionSuccessful();

            Map<String, ArrayList> setClasses = new HashMap<String, ArrayList>();
            if(cursor.moveToFirst())
                do {
                    ArrayList<String> classDetails = new ArrayList<String>();

                    //add COLUMN ID to list
                    classDetails.add(cursor.getString(0));
                    //add CLASS_NAME to list
                    classDetails.add(cursor.getString(1));
                    //add CLASS_DATE to list
                    classDetails.add(cursor.getString(2));
                    //add CLASS_TIME to list
                    classDetails.add(cursor.getString(3));
                    //add CLASS_DURATION to list
                    classDetails.add(cursor.getString(4));
                    //add CLASS_INSTRUCTOR to list
                    classDetails.add(cursor.getString(5));
                    //add CLASS_CAPACITY to list
                    classDetails.add(cursor.getString(6));
                    //add CLASS_AVAILABILITY to list
                    classDetails.add(cursor.getString(7));

                    setClasses.put(cursor.getString(0), classDetails);

                }while (cursor.moveToNext());
            else
                Log.i("Cursor returned value", "false");

            return setClasses;

        } catch (Exception e){
            Log.e("getClassesDetails", "Failed to complete transaction");
            return null;
            //Error in between database transaction
        } finally {
            db.endTransaction();
        }

    }

    public void addClasses(){

        SQLiteDatabase db = getWritableDatabase();
        long rows_1, rows_2,rows_3, rows_4, rows_5, rows_6, rows_7, rows_8, rows_9, rows_10, rows_11, tot_rows_inserted;

        ContentValues h_health_hub = new ContentValues();
        h_health_hub.put(CLASS_COLUMN_ID, 1);
        h_health_hub.put(CLASS_NAME, "Holistic Health Hub");
        h_health_hub.put(CLASS_DATE, "12/04/2023");
        h_health_hub.put(CLASS_TIME, "16:00");
        h_health_hub.put(CLASS_DURATION,"20");
        h_health_hub.put(CLASS_INSTRUCTOR, "Alex Walker");
        h_health_hub.put(CLASS_CAPACITY, 6);
        h_health_hub.put(CLASS_AVAILABILITY, 6);

        ContentValues you_heads_win = new ContentValues();
        you_heads_win.put(CLASS_COLUMN_ID, 2);
        you_heads_win.put(CLASS_NAME, "Heads You Win");
        you_heads_win.put(CLASS_DATE, "07/04/2023");
        you_heads_win.put(CLASS_TIME, "05:00");
        you_heads_win.put(CLASS_DURATION,"15");
        you_heads_win.put(CLASS_INSTRUCTOR, "Jeff Earth");
        you_heads_win.put(CLASS_CAPACITY, 12);
        you_heads_win.put(CLASS_AVAILABILITY, 12);

        ContentValues crossFit = new ContentValues();
        crossFit.put(CLASS_COLUMN_ID, 3);
        crossFit.put(CLASS_NAME, "Spark CrossFit");
        crossFit.put(CLASS_DATE, "12/04/2023");
        crossFit.put(CLASS_TIME, "07:00");
        crossFit.put(CLASS_DURATION,"30");
        crossFit.put(CLASS_INSTRUCTOR, "Luna Williams");
        crossFit.put(CLASS_CAPACITY, 7);
        crossFit.put(CLASS_AVAILABILITY, 7);

        ContentValues dangerGym = new ContentValues();
        dangerGym.put(CLASS_COLUMN_ID, 4);
        dangerGym.put(CLASS_NAME, "Danger Gym");
        dangerGym.put(CLASS_DATE, "25/04/2023");
        dangerGym.put(CLASS_TIME, "13:00");
        dangerGym.put(CLASS_DURATION,"20");
        dangerGym.put(CLASS_INSTRUCTOR, "John Asare");
        dangerGym.put(CLASS_CAPACITY, 10);
        dangerGym.put(CLASS_AVAILABILITY, 10);
//
        ContentValues Acore = new ContentValues();
        Acore.put(CLASS_COLUMN_ID, 5);
        Acore.put(CLASS_NAME, "Aerial Core");
        Acore.put(CLASS_DATE, "16/04/2023");
        Acore.put(CLASS_TIME, "20:00");
        Acore.put(CLASS_DURATION,"15");
        Acore.put(CLASS_INSTRUCTOR, "Nichola Martin");
        Acore.put(CLASS_CAPACITY, 5);
        Acore.put(CLASS_AVAILABILITY, 5);
//
        ContentValues shapers = new ContentValues();
        shapers.put(CLASS_COLUMN_ID, 6);
        shapers.put(CLASS_NAME, "Rapid Fitness Re shapers");
        shapers.put(CLASS_DATE, "30/03/2023");
        shapers.put(CLASS_TIME, "06:00");
        shapers.put(CLASS_DURATION,"25");
        shapers.put(CLASS_INSTRUCTOR, "Antesar Shabut");
        shapers.put(CLASS_CAPACITY, 15);
        shapers.put(CLASS_AVAILABILITY, 15);
//
        ContentValues serious = new ContentValues();
        serious.put(CLASS_COLUMN_ID, 7);
        serious.put(CLASS_NAME, "Serious Work Outs");
        serious.put(CLASS_DATE, "02/04/2023");
        serious.put(CLASS_TIME, "18:00");
        serious.put(CLASS_DURATION,"30");
        serious.put(CLASS_INSTRUCTOR, "Jim Docker");
        serious.put(CLASS_CAPACITY, 6);
        serious.put(CLASS_AVAILABILITY, 6);
//
        ContentValues workout = new ContentValues();
        workout.put(CLASS_COLUMN_ID, 8);
        workout.put(CLASS_NAME, "Wake And Workout");
        workout.put(CLASS_DATE, "13/04/2023");
        workout.put(CLASS_TIME, "20:00");
        workout.put(CLASS_DURATION,"15");
        workout.put(CLASS_INSTRUCTOR, "Nichola Martin");
        workout.put(CLASS_CAPACITY, 5);
        workout.put(CLASS_AVAILABILITY, 5);
//
        ContentValues r_shapers = new ContentValues();
        r_shapers.put(CLASS_COLUMN_ID, 9);
        r_shapers.put(CLASS_NAME, "Rapid Fitness Re shapers");
        r_shapers.put(CLASS_DATE, "30/03/2023");
        r_shapers.put(CLASS_TIME, "06:00");
        r_shapers.put(CLASS_DURATION,"25");
        r_shapers.put(CLASS_INSTRUCTOR, "Antesar Shabut");
        r_shapers.put(CLASS_CAPACITY, 15);
        r_shapers.put(CLASS_AVAILABILITY, 15);

        ContentValues health_hub = new ContentValues();
        health_hub.put(CLASS_COLUMN_ID, 10);
        health_hub.put(CLASS_NAME, "Holistic Health Hub");
        health_hub.put(CLASS_DATE, "02/05/2023");
        health_hub.put(CLASS_TIME, "18:00");
        health_hub.put(CLASS_DURATION,"30");
        health_hub.put(CLASS_INSTRUCTOR, "Jim Docker");
        health_hub.put(CLASS_CAPACITY, 6);
        health_hub.put(CLASS_AVAILABILITY, 6);

        ContentValues heads_win = new ContentValues();
        heads_win.put(CLASS_COLUMN_ID, 11);
        heads_win.put(CLASS_NAME, "Heads You Win");
        heads_win.put(CLASS_DATE, "04/04/2023");
        heads_win.put(CLASS_TIME, "15:00");
        heads_win.put(CLASS_DURATION,"15");
        heads_win.put(CLASS_INSTRUCTOR, "Jim Gipton");
        heads_win.put(CLASS_CAPACITY, 12);
        heads_win.put(CLASS_AVAILABILITY, 12);

        try{

            rows_1 = db.insert(TABLE_CLASSES, null, h_health_hub);
            rows_2 = db.insert(TABLE_CLASSES, null, you_heads_win);
            rows_3 = db.insert(TABLE_CLASSES, null, crossFit);
            rows_4 = db.insert(TABLE_CLASSES, null, dangerGym);
            rows_5 = db.insert(TABLE_CLASSES, null, Acore);
            rows_6 = db.insert(TABLE_CLASSES, null, shapers);
            rows_7 = db.insert(TABLE_CLASSES, null, serious);
            rows_8 = db.insert(TABLE_CLASSES, null, workout);
            rows_9 = db.insert(TABLE_CLASSES, null, r_shapers);
            rows_10 = db.insert(TABLE_CLASSES, null, health_hub);
            rows_11 = db.insert(TABLE_CLASSES, null, heads_win);

            tot_rows_inserted = rows_1 + rows_2 + rows_3 + rows_3 + rows_4 + rows_5 +
                    rows_6 + rows_7 + rows_8 + rows_9 + rows_10 + rows_11;

          Log.d("Inserted rows", String.valueOf(tot_rows_inserted));

          //return  tot_rows_inserted;
        } catch (Exception e) {
            Log.e("Error during insert", e.toString());
            //return -1;
        }
    }
    public boolean updateTable(String updateQuery){

        SQLiteDatabase db = getWritableDatabase();

        try {
            db.execSQL(updateQuery);
            Log.d("update msg", "done updating!");
            return true;
        } catch (Exception e) {
            Log.d("update msg", e.toString());
            return false;
        }
    }

    public boolean alreadyBooked(String username, String className){

        SQLiteDatabase db = getReadableDatabase();

        boolean authRes = false;

        String fetchuser = "Select " + USER_CLASSES_USERNAME + ", " + USER_CLASSES_CLASSESNAME + " from " + TABLE_USER_CLASSES;
        Cursor cursor = db.rawQuery(fetchuser, null);
        String a,b ="not found";

        System.out.println("username: " + username);
        System.out.println("Class name: " + className);
        if(cursor.moveToFirst()){
            Log.d("Select ", " clause");

            do{
                a = cursor.getString(0);
                b = cursor.getString(1);
                Log.d("a ", a);
                if(a.equals(username) && b.equals(className)){
                    authRes = true;
                    break;
                }
            } while(cursor.moveToNext());

        }
        if (authRes) {
            return true;
        }
        else{
            return false;
        }

    }


    public void selectInsertUserClasses(String username, String className, String status){

        SQLiteDatabase db_write = getWritableDatabase();
        SQLiteDatabase db_read = getReadableDatabase();

        String selectUser = "SELECT rowid FROM user WHERE username='" + username + "'";
        String selectClass = "SELECT rowid FROM classes WHERE name='" + className + "'";

        Cursor userInfo = db_read.rawQuery(selectUser, null);
        Cursor classInfo = db_read.rawQuery(selectClass, null);

        String userId = null;
        String classId = null;

        if(userInfo.moveToFirst()){
            do{
                userId = userInfo.getString(0);
            } while (userInfo.moveToNext());
        }

        if(classInfo.moveToFirst()){
            do{
                classId = classInfo.getString(0);
            } while (classInfo.moveToNext());
        }

        if(userId!=null && classId!=null){
            try{

                ContentValues userClassValues = new ContentValues();
                userClassValues.put(USER_CLASSES_USERID, userId);
                userClassValues.put(USER_CLASSES_USERNAME, username);
                userClassValues.put(USER_CLASSES_CLASSESID, classId);
                userClassValues.put(USER_CLASSES_CLASSESNAME, className);
                userClassValues.put(USER_CLASSES_STATUS, status);

                db_write.insert(TABLE_USER_CLASSES, null, userClassValues);

                Log.d(TABLE_USER_CLASSES, "Inserted successfully!");

            } catch (Exception e) {
                Log.e("Error during insert", e.toString());
            }
        }
    }

}