package com.example.ltufitnessdesign;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ForgotPasswordActivity extends AppCompatActivity {

    DataBaseActivity ltuDB = new DataBaseActivity(this);
    private static final int PASSWORD_LENGTH = 6;
    EditText email, newPassword;
    Button restPassword, showBtn, restLoginBtn;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        email = findViewById(R.id.restUserEmail);
        newPassword = findViewById(R.id.restUserNewPassword);

        restPassword = findViewById(R.id.restUserButton);
        restPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restNewPassword();
            }
        });

        showBtn = findViewById(R.id.showPassword);
        showBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (newPassword.getText().toString().isEmpty()) {
                    newPassword.setError("Please Enter Password");
                } else {
                    if (showBtn.getText().toString().equals("Show")) {
                        showBtn.setText("Hide");
                        newPassword.setTransformationMethod(null);
                    } else {
                        showBtn.setText("Show");
                        newPassword.setTransformationMethod(new PasswordTransformationMethod());
                    }
                }
            }
        });

        restLoginBtn = findViewById(R.id.restLoginBtn);

        restLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent login = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
                startActivity(login);
            }
        });

    }


    public void restNewPassword(){

        String userEmail = email.getText().toString().trim();
        String nPassword = newPassword.getText().toString().trim();

        if (userEmail.isEmpty()) {
            email.setError("Email is required");
            email.requestFocus();
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(userEmail).matches()) {
            email.setError("Please provide a valid username!");
            email.requestFocus();
            return;
        }
        if (!updatePassword(userEmail,nPassword)) {
            email.setError("Incorrect email! Insert correct email to reset password.");
            email.requestFocus();
            return;
        }

        if (nPassword.isEmpty()) {
            newPassword.setError("Password is required");
            newPassword.requestFocus();
            return;
        }
        if (nPassword.length() < PASSWORD_LENGTH) {
            newPassword.setError("Password is required");
            newPassword.requestFocus();
            return;
        }

        if(updatePassword(userEmail,nPassword)){
            try {
                Intent login = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
                startActivity(login);
            } catch (Exception e){
                Log.d("Error Msg ", e.toString());
            }

        }
        else {
            Toast.makeText(this, "Unsuccessful change of password!", Toast.LENGTH_LONG).show();
        }

    }

    public boolean updatePassword(String email, String newPassword) {

        String select = "SELECT id, email FROM user WHERE email='" + email +"'";
        Cursor cursor = ltuDB.selectOps(select);
        System.out.println("Find count: " + cursor.getCount());

        cursor.moveToFirst();
        String id_str = cursor.getString(0);
        System.out.println("Find count: " + cursor.getString(0));
        System.out.println("Find Email related: " + cursor.getString(1));
        int id = Integer.parseInt(id_str);


        boolean emailExist = ltuDB.checkEmailExist(email, newPassword);

        boolean updateStatus = false;

        if(emailExist){
            String query = "UPDATE user SET password='" + newPassword + "' WHERE id=" + id;
            updateStatus = ltuDB.updateTable(query);
            Log.i("update response: ", String.valueOf(updateStatus));
            Log.i("update response: ", String.valueOf(ltuDB.updateTable(query)));
        }

        return updateStatus;
    }
}