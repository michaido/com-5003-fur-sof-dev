package com.example.ltufitnessdesign;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

public class DataBaseManager extends SQLiteOpenHelper {

    static SQLiteDatabase db;
    private static String DATABASE_NAME;
    private static int DATABASE_VERSION;
    private Context context;

    public DataBaseManager(Context context, String dataBaseName, @Nullable SQLiteDatabase.CursorFactory factory, int version) {

        super(context, dataBaseName, factory, version);
        this.context = context;
        this.DATABASE_NAME = dataBaseName;
        this.DATABASE_VERSION = version;

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        this.db = db;
        Log.d("Database Connection","connected");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public SQLiteDatabase returnDB() {
        Log.d("Database Return","returned");
        return db;

    }


    public void createTable(String createTableQuery){
        db.execSQL(createTableQuery);
        Log.d("Table created","created");

    }

    public void deleteTable(String deleteTableQuery){
        db.execSQL(deleteTableQuery);
        Log.d("Table Delete","Deleted");
    }

    public void dbDisconnect(){
        //this.db = db;
        db.close();
        Log.d("Database Connection","Closed");

    }

}
