package com.example.ltufitnessdesign;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class BookFitnessClasses extends AppCompatActivity {

    DataBaseActivity ltuDB = new DataBaseActivity(this);
    TextView className, classDate, classInstructor, classTime, classAvailability, classDuration;
    Button bookClass;
    ImageView back, home;
    //UserInfo user = new UserInfo();

    Intent item = getIntent();
    String username = item.getStringExtra("username");



    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_fitness_classes);

        //user.setUsername();
        back = findViewById(R.id.backToList);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent listView = new Intent(BookFitnessClasses.this, FitnessClassesActivity.class);
                System.out.println("username value from : " + username);
                startActivity(listView);
            }
        });

        className = findViewById(R.id.className);
        classDate = findViewById(R.id.classDate);
        classInstructor = findViewById(R.id.classInstructor);
        classTime = findViewById(R.id.classTime);
        classDuration = findViewById(R.id.classDuration);
        classAvailability = findViewById(R.id.classAvailability);

        className.setKeyListener(null);
        classDate.setKeyListener(null);
        classInstructor.setKeyListener(null);
        classTime.setKeyListener(null);
        classAvailability.setKeyListener(null);


        String id = item.getStringExtra("id");
        String name = item.getStringExtra("name");
        String date = item.getStringExtra("date");
        String time = item.getStringExtra("time");
        String duration = item.getStringExtra("duration");
        String instructor = item.getStringExtra("instructor");


        System.out.println("username value from FitnessClasses: " + username);
        final String[] availability = {item.getStringExtra("availability")};


        className.setText(name);
        classDate.setText(date);
        classTime.setText(time);
        classDuration.setText(duration);
        classInstructor.setText(instructor);
        classAvailability.setText(availability[0]);

        home = findViewById(R.id.homeBtn);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(BookFitnessClasses.this, HomeActivity.class);
                startActivity(home);
            }
        });


        bookClass = findViewById(R.id.bookClasse);
        bookClass.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String selectAvailability = getSelectQuery(name, Integer.parseInt(id));
                if(bookClass.getText().toString().equals("Book class")){
                    int spaceDecreased = decreaseSpace(availability[0]);

                    String updateQuery = getUpdateQuery(spaceDecreased, name, Integer.parseInt(id));

                    if(ltuDB.updateTable(updateQuery)) {
                        Cursor cursor = ltuDB.selectOps(selectAvailability);
                        String currentSpaceAvailable = getQueryResult(cursor, Integer.parseInt(id), name);
                        availability[0] = currentSpaceAvailable;
                        classAvailability.setText("Available Space: " + currentSpaceAvailable);

                        if(ltuDB.alreadyBooked(username,name)){
                            ltuDB.updateTable(updateUserClassStatus("Cancel", username, name));
                        }else{
                            ltuDB.selectInsertUserClasses(username, name, "Cancel");
                        }

                        Cursor selectStatus_1 = ltuDB.selectOps(selectStatus(username, name));
                        String classStatus_1 = getClassStatusResult(selectStatus_1, username, name);
                        bookClass.setText(classStatus_1);

                    } else {
                        Toast.makeText(BookFitnessClasses.this, "Something went wrong updating Classes table during booking", Toast.LENGTH_LONG).show();
                    }
                }else if (bookClass.getText().toString().equals("Cancel")){
                    int spaceIncrease = increaseSpace(availability[0]);
                    String updateQuery = getUpdateQuery(spaceIncrease, name, Integer.parseInt(id));

                    if(ltuDB.updateTable(updateQuery)) {
                        Cursor cursor = ltuDB.selectOps(getSelectQuery(name, Integer.parseInt(id)));
                        String currentSpaceAvailable = getQueryResult(cursor, Integer.parseInt(id), name);
                        availability[0] = currentSpaceAvailable;
                        classAvailability.setText("Available Space: " + currentSpaceAvailable);

                        if(ltuDB.alreadyBooked(username,name)){
                            ltuDB.updateTable(updateUserClassStatus("Book class", username, name));
                        }

                        Cursor selectStatus = ltuDB.selectOps(selectStatus(username, name));
                        String classStatus = getClassStatusResult(selectStatus, username, name);
                        bookClass.setText(classStatus);

                    } else {
                        Toast.makeText(BookFitnessClasses.this,
                                "Something went wrong updating Classes table during canceling of booking", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(BookFitnessClasses.this,
                            "Something went wrong while clicking on the book", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public int increaseSpace(String space){
        String numSpace[] = space.split(":");
        String spaceValue = numSpace[0].trim();

        if(numSpace.length==2)
            spaceValue = numSpace[1].trim();
        else{
            spaceValue = numSpace[0];
        }

        System.out.println(" Value of availability: " + spaceValue);
        int spaceAvailable = Integer.parseInt(spaceValue);
        spaceAvailable = spaceAvailable +1;

        return spaceAvailable;
    }

    public int decreaseSpace(String space){
        String numSpace[] = space.split(":");
        String spaceValue;
        if(numSpace.length==2)
            spaceValue = numSpace[1].trim();
        else{
            spaceValue = numSpace[0];
        }

        int spaceAvailable = Integer.parseInt(spaceValue);
        spaceAvailable = spaceAvailable -1;

        return spaceAvailable;
    }

    public String getSelectQuery(String name, int rowId){
        String query = "SELECT id, name, availability  FROM classes WHERE id= " + rowId + " AND name='" + name+ "'";
        return query;
    }

    public String getUpdateQuery(int spaceVaule, String name, int rowId){
        String query = "UPDATE classes SET availability=" + spaceVaule + " WHERE id= " + rowId + " and name='" + name+ "'";
        return query;
    }


    public String selectStatus(String username, String classesName){

        String select_user_classes = "SELECT status, username, classesName FROM user_classes WHERE username='" + username + "' AND classesName='" + classesName + "'";

        return select_user_classes;
    }

    public String updateUserClassStatus(String status, String username, String className){
        String updateStatusColumn = "UPDATE user_classes SET status='" + status + "' WHERE username='" + username + "' AND classesName='" + className + "'";

        return updateStatusColumn;
    }

    public String getQueryResult(Cursor cursor, Integer rowId, String name){
        String queryResId,queryResName, queryResAvailability ="not found";
        if(cursor.moveToFirst()){
            //Log.d("Select " , " clause");
            do{
                queryResId = cursor.getString(0);
                queryResName = cursor.getString(1);
                Log.d("queryResId " , queryResId);
                Log.d("queryResName " , queryResName);
                Log.d("rowId " , String.valueOf(rowId));
                Log.d("rowId " , name);
                if (queryResId.equals(String.valueOf(rowId)) && queryResName.equals(name)){
                    //Log.d("username If loop" , a);
                    queryResAvailability = cursor.getString(2);
                    //classAvailability.setText("" + queryResAvailability);
                    Log.d("queryResAvailability " , queryResAvailability);
                    break;
                }
            }
            while(cursor.moveToNext());
        }

        return queryResAvailability;

    }

    public String getClassStatusResult(Cursor cursor, String username, String name){
        String status = "not found";
        String resUsername, resClassesName = "not found";
        if(cursor.moveToFirst()){
            //Log.d("Select " , " clause");
            do{
                resUsername = cursor.getString(1);
                resClassesName = cursor.getString(2);
                if (resUsername.equals(String.valueOf(username)) && resClassesName.equals(name)){
                    //Log.d("username If loop" , a);
                    status = cursor.getString(0);
                    //classAvailability.setText("" + queryResAvailability);
                    Log.d("Class status for " + resUsername + " is " , status);
                    break;
                }
            }
            while(cursor.moveToNext());
        }

        return status;

    }

}